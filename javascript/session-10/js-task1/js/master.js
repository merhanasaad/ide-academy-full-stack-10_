"use strict";

const _ = document;

function sum() {
  // logic
  const n1 = _.getElementById("number1").value,
    n2 = _.getElementById("number2").value;
  // validations
  let error = false;
  // isNaN(n1) return true if NaN , return true if is number

  if (isNaN(n1)) {
    _.getElementById("number1-errors").innerText = "this not a valid number";
    error = true;
  } else {
    _.getElementById("number1-errors").innerText = "";
  }

  if (isNaN(n2)) {
    _.getElementById("number2-errors").innerText = "this not a valid number";
    error = true;
  } else {
    _.getElementById("number2-errors").innerText = "";
  }
  // end validations

  if (error) {
    return "";
  }

  return parseFloat(n1) + parseFloat(n2);
}

// Events (Actions)
_.addEventListener("DOMContentLoaded", function() {
  const btn = _.getElementById("btn-result");
  btn.addEventListener("click", btnResult);

  const demo = _.getElementById("demo");
  demo.addEventListener("click", () => alert("demo"));
});

// ES 6
const btnResult = () => {
  _.getElementById("result").innerHTML = `<h1 id="result-value">${sum()}</h1>`;

  //   const resultValue = _.getElementById('result-value');
  //   resultValue.addEventListener("click", () => alert('result-value'));
};

//task-solution
const observer = new MutationObserver(mutation => {
  const resultValue = _.getElementById("result-value");
  if (resultValue) {
    resultValue.addEventListener("click", () => alert("result-value"));
  }
  console.log("result mutation detected");
});

// Options for the observer (which mutations to observe)
observer.observe(_.getElementById("result"), {
  childList: true,
  attributes: true,
  subtree: true
});
