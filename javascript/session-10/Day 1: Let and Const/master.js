const PI = Math.PI;

function calcArea(r) {
  const validR = validRedius(r);

  if (validR) {
    return PI * (r * r);
  } else {
    console.log("radius not valid");
  }
}

function calcPerimeter(r) {
  const validR = validRedius(r);

  if (validR) {
    return 2 * PI * r;
  } else {
    console.log("radius not valid");
  }
}

function validRedius(r) {
  if (r > 0 && r <= 100) {
    return true;
  } else {
    return false;
  }
}

let radius = 2.6;
let area = calcArea(radius);
let perimeter = calcPerimeter(radius);

console.log(area);
console.log(perimeter);
