const modifyArray = ((nums) => {

    let modifiedArray = [];

    nums.map(num => {
        if (num < 100 && num > 0) {
            if (num % 2 == 0) {
                modifiedArray.push(num * 2);
            } else {
                modifiedArray.push(num * 3)
            }
        } else {
            return "nums element is not valid";
        }
    });

    return modifiedArray;
});


const n = 5;
const arrayOfnums = [1, 2, 3, 4, 5];
let test;

if (arrayOfnums.length === n && n > 0 && n <= 10) {
    test = modifyArray(arrayOfnums);
} else {
    console.log("check the array length = " + n + "and n between 1 to 10")
}

console.log(test);