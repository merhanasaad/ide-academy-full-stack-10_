'use strict'

let counter = 0;
const btn = document.getElementById("btn");

btn.addEventListener("click", () => {
    counter += 1;
    btn.innerText = counter;
});