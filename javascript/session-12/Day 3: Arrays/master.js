function getSecondLargest(nums) {

    //arrange nums from large to small
    let arrangedNums = nums.sort(function (a, b) {
        return b - a
    });

    //check the validation of nums item 
    nums.map(num => {
        if (!(num < 100 && num > 0)) {
            return "nums element is not valid";
        }
    });
    //remove one of the repeated numbers if found
    for (let i = 0; i < arrangedNums.length; i++) {
        for (let j = i + 1; j < arrangedNums.length; j++) {
            if (arrangedNums[i] === arrangedNums[j]) {
                arrangedNums.splice(i, 1);
            }
        }
    }
    return arrangedNums[1];
}


const n = 10;
const arrayOfnums = [10, 9, 9, 8, 8, 11, 8, 0, 9, 1];
let test;

if (arrayOfnums.length === n && n > 0 && n <= 10) {
    test = getSecondLargest(arrayOfnums); //[6, 5, 3]
} else {
    console.log("check the array length = " + n + "and n between 1 to 10")
}

console.log(test);