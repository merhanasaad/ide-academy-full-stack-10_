const employeeSalary = ((baseSalary) => {
    let salary = baseSalary;

    function totalSalary() {
        return salary
    };

    function bonus(_bouns) {
        salary += _bouns;
        return salary
    };

    function tax(_tax) {
        salary -= _tax;
        return salary
    };
    return {
        bonus: bonus,
        tax: tax,
        totalSalary: totalSalary
    }
});

const empSalary = employeeSalary(3000);

console.log(empSalary.bonus(150)); //3150
console.log(empSalary.tax(70)); //3080
console.log(empSalary.totalSalary()); //3080