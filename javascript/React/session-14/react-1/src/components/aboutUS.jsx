import React from 'react';

const aboutUS = () => (
    <div className="about-us" id="About">  
        <div className="container text-center">
            <h3 className="w-100">About Us</h3>
            <p className="w-75 font-italic pb-5 m-auto">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate aut at veritatis officia hic earum veniam dolorem inventore architecto consectetur.</p>
            <div className="row pb-5">
                <div className="col card mr-4 pb-5">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                    </div>
                </div>
                <div className="col card mr-4 pb-5">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                    </div>
                </div>
                <div className="col card pb-5">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                    </div>
                </div>
            </div>
            <button class="btn bg-main-color rounded-pill">Click Here</button>
        </div>
    </div>
);

export default aboutUS;