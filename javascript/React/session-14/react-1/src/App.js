import React, { Fragment } from 'react';
import Header from './components/header';
import AboutUs from './components/aboutUS'
import ContactUs from './components/contactUs';
import Footer from './components/footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css'
import './App.css';



const App = () => ( 
  <Fragment>
    <Header/>
    <AboutUs/>
    <ContactUs/>
    <Footer/>
  </Fragment>
);

export default App;